class Article < ActiveRecord::Base
  attr_accessible :content, :topic, :user_id, :tag_ids
  belongs_to :user
  has_many :article_tag_relations,:dependent=>:destroy
  has_many :tags ,:through => :article_tag_relations
end
