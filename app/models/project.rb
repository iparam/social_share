class Project < ActiveRecord::Base
  attr_accessible :category, :current_duration, :current_end_date, :current_expected_cost, :current_start_date, :desc, :duration_unit, :expected_duration, :expected_end_date, :expected_skill_level, :expected_start_date, :initial_expected_cost, :name, :percentage_of_completion, :status
end
