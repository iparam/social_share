class ArticleTagRelation < ActiveRecord::Base
  attr_accessible :article_id, :tag_id
  belongs_to :tag
  belongs_to :article
end
