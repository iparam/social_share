class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.integer :user_id
      t.integer :project_id
      t.text :desc
      t.string :title
      t.string :type
      t.string :status

      t.timestamps
    end
  end
end
