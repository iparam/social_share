class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.string :name
      t.text :desc
      t.date :expected_start_date
      t.date :expected_end_date
      t.string :expected_duration
      t.date :current_start_date
      t.date :current_end_date
      t.string :current_duration
      t.integer :duration_unit
      t.float :initial_expected_cost
      t.float :current_expected_cost
      t.string :expected_skill_level
      t.string :category
      t.float :percentage_of_completion
      t.string :status

      t.timestamps
    end
  end
end
